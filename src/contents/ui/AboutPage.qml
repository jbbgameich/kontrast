/**
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 * 
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.1
import org.kde.kirigami 2.19 as Kirigami
import org.kde.kontrast.private 1.0
import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm

MobileForm.AboutPage {
    aboutData: About
}
